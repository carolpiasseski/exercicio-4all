import React from 'react';
import { View, Image, Text, StyleSheet } from 'react-native';
import StarRating from 'react-native-star-rating';
import colors from '../constants/colors';

const Comment = (props) => (
    <View style={styles.commentContainer}>
        <View style={styles.photoContainer}>
            <View style={{ height: 80, width: 80 }}>
                <Image source={{ uri: props.urlFoto }} style={{ borderRadius: 40, flex: 1 }} />
            </View>
        </View>
        <View style={{ flex: 3 }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <View style={{ flex: 3, padding: 3 }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 2 }}>
                            <Text style={styles.textColor}>
                                {props.nome}
                            </Text>
                            <Text style={styles.textColor}>
                                {props.titulo}
                            </Text>
                        </View>
                        <View style={styles.starContainer}>
                            <StarRating
                                style={{ flex: 1 }}
                                starSize={15}
                                fullStarColor={colors.geebung}
                                emptyStarColor={colors.geebung}
                                disabled={true}
                                maxStars={5}
                                rating={props.nota}
                            />
                        </View>
                    </View>
                    <Text style={{ color: colors.geebung }}>
                        {props.comentario}
                    </Text>
                </View>
            </View>
        </View>
    </View>
)

const styles = StyleSheet.create({
    commentContainer: {
        flex: 1, 
        flexDirection: 'row', 
        minHeight: 100, 
        backgroundColor: colors.white, 
        marginTop: 2
    },
    photoContainer: {
        flex: 1, 
        padding: 10, 
        alignItems: 'center', 
        justifyContent: 'center'
    },
    textColor: {
        color: colors.geebung
    },
    starContainer: {
        flex: 1, 
        justifyContent: 'center', 
        marginRight: 5
    }
});

export default Comment;