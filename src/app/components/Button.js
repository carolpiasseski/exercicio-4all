import React from 'react';
import{ TouchableOpacity, View, Image, Text, StyleSheet } from 'react-native';
import colors from '../constants/colors';

const Button = (props) => (
    <TouchableOpacity
        style={styles.buttonTouchable}
        onPress={() => props.onPress()}
    >
        <View style={styles.imageContainer}>
            <Image
                source={props.source}
                resizeMode={'contain'}
                style={styles.buttonImage}
            />
        </View>
        <Text style={styles.buttonText}>{props.text}</Text>
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    buttonTouchable: {
        flex: 1,
        padding: 4
    },
    imageContainer: {
        flex: 4
    },
    buttonImage: {
        flex: 1,
        alignSelf: 'center'
    },
    buttonText: {
        flex: 2,
        textAlign: 'center',
        fontSize: 11,
        color: colors.geebung
    },
});

export default Button;