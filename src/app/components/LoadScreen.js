import React from 'react';
import { Text, View, ActivityIndicator, StyleSheet } from 'react-native';
import colors from '../constants/colors';

const LoadScreen = props => (
    <View style={styles.ViewStyle}>
        <ActivityIndicator color={colors.geebung} size={64} />
        <Text style={styles.TextStyle}>Carregando...</Text>
    </View>
)

const styles = StyleSheet.create({
    ViewStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    TextStyle: {
        textAlign: 'center',
        marginTop: 10,
        fontSize: 20
    },
});

export default LoadScreen;