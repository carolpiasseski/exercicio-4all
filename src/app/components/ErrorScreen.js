import React from 'react';
import { Text, ScrollView, RefreshControl, StyleSheet } from 'react-native';

const ErrorScreen = props => (
    <ScrollView
        contentContainerStyle={styles.contentContainer}
        refreshControl={
            <RefreshControl
                refreshing={props.isRefreshing}
                onRefresh={props.onRefresh}
            />
        }
    >
        <Text style={styles.FirstTextStyle}>
            Ocorreu um erro.
        </Text>
        <Text style={styles.SecondTextStyle}>
            Arraste a tela para baixo para recarregar.
        </Text>
    </ScrollView>
)

const styles = StyleSheet.create({
    contentContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center' 
    },
    FirstTextStyle: {
        fontSize: 20
    },
    SecondTextStyle: {
        fontSize: 18,
        fontWeight: 'bold'
    },
});

export default ErrorScreen;
