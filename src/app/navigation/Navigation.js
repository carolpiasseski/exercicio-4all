import HomeScreen from '../screens/HomeScreen';
import DetailScreen from '../screens/DetailScreen/DetailScreen';
import ServiceScreen from '../screens/ServiceScreen';
import { createStackNavigator, createAppContainer } from 'react-navigation';

const AppNavigator = createStackNavigator(
    {
        'Home': {
            screen: HomeScreen
        },
        'Detail': {
            screen: DetailScreen
        },
        'Service': {
            screen: ServiceScreen
        }
    },
    {
        initialRouteName: 'Home'
    } 
);

export default createAppContainer(AppNavigator);
