import React, { Component } from 'react';
import { View, StatusBar } from 'react-native';
import NavigationContainer from './navigation/Navigation';

class App extends Component {
    render() {
        return(
            <View style={{ flex: 1 }}>
                <NavigationContainer />
                <StatusBar 
                    backgroundColor = '#cb8a19'
                    barStyle = "lightContent"
                />
         </View>
        )
    }
} 

export default App;