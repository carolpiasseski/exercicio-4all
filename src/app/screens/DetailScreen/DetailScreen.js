import React, { Component } from 'react';
import { ScrollView, Text, View, TouchableOpacity, Image, Alert } from 'react-native';
import Communications from 'react-native-communications';
import MapView, { Marker } from 'react-native-maps';
import axios from 'axios';
import LoadScreen from '../../components/LoadScreen';
import ErrorScreen from '../../components/ErrorScreen';
import Button from '../../components/Button';
import Comment from '../../components/Comment';
import styles from './styles';
import colors from '../../constants/colors';
import ligar from '../../assets/ligar.png';
import servicos from '../../assets/servicos.png';
import comentarios from '../../assets/comentarios.png';
import favoritos from '../../assets/favoritos.png';
import endereco from '../../assets/endereco.png';
import search from '../../assets/search.png';

class DetailScreen extends Component {
    state = {
        loading: true,
        error: false
    }

    componentWillMount() {
        this.loadData()
    }

    loadData = async () => {
        this.setState({
            loading: true
        })

        try {
            const itemId = this.props.navigation.getParam('id')
            const response = await axios.get('http://dev.4all.com:3003/tarefa/' + itemId)
            this.setState({
                loading: false,
                error: false,
                data: response.data
            })
            this.props.navigation.setParams({
                title: `${response.data.cidade} - ${response.data.bairro}`,
            });
        } catch (err) {
            this.setState({
                loading: false,
                error: true
            })
        }
    }

    static navigationOptions = ({ navigation }) => {
        const title = navigation.getParam('title', 'Carregando...')
        return {
            title: title,
            headerStyle: { backgroundColor: colors.geebung, height: 40 },
            headerTintColor: colors.white,
            headerTitleStyle: { fontSize: 13, fontWeight: 'bold', textAlign: "center", flex: 1 },
            headerRight: (
                <TouchableOpacity style={{ flex: 1, paddingVertical: 10, justifyContent: 'flex-end' }}>
                    <Image source={search} style={{ flex: 1 }} resizeMode={'contain'} />
                </TouchableOpacity>
            )
        };
    };

    render() {
        if (this.state.loading) {
            return <LoadScreen />
        }
        if (this.state.error) {
            return <ErrorScreen isRefreshing={this.state.loading} onRefresh={this.loadData} />
        }

        return (
            <ScrollView ref={(ref) => this.scrollView = ref} style={styles.scrollView}>
                <Image
                    source={{ uri: this.state.data.urlFoto }}
                    resizeMode={'cover'}
                    style={styles.photoImageBackground}
                />

                <View style={styles.logoContainer}>
                    <Image
                        source={{ uri: this.state.data.urlLogo }}
                        resizeMode={'contain'}
                        style={styles.logoImage}
                    >
                    </Image>
                </View>

                <View>
                    <Text style={styles.titleText}>{this.state.data.titulo}</Text>
                </View>

                <View style={styles.buttonBarContainer}>
                    <Button onPress={() => Communications.phonecall(this.state.data.telefone, true)} source={ligar} text={'Ligar'}/>
                    <Button onPress={() => this.props.navigation.navigate('Service')} source={servicos} text={'Serviços'}/>
                    <Button onPress={() => Alert.alert('Endereço', this.state.data.endereco)} source={endereco} text={'Endereço'}/>
                    <Button onPress={() => this.scrollView.scrollToEnd()} source={comentarios} text={'Comentários'}/>
                    <Button onPress={() => { }} source={favoritos} text={'Favoritos'}/>
                </View>

                <View style={styles.descriptionContainer}>
                    <Text style={styles.descriptionText}>{this.state.data.texto}</Text>
                </View>

                <View style={styles.mapContainer}>
                    <MapView
                        style={styles.mapStyle}
                        //scrollEnabled={false}
                        region={{
                            latitude: this.state.data.latitude,
                            longitude: this.state.data.longitude,
                            latitudeDelta: 0.015,
                            longitudeDelta: 0.0121,
                        }}
                    >
                        <Marker
                            coordinate={{
                                latitude: this.state.data.latitude,
                                longitude: this.state.data.longitude
                            }}
                        />
                    </MapView>
                </View>
                <View style={styles.addressContainer}>
                    <Text style={styles.addressText}>
                        {this.state.data.endereco}
                    </Text>
                    <View style={styles.adressContainerLogo}>
                        <Image
                            source={endereco}
                            resizeMode={'contain'}
                            style={styles.addressImage}
                        />
                    </View>
                </View>

                {this.state.data.comentarios.map((item, index) => <Comment key={index} {...item} />)}
                <View style={styles.emptyContainer} />
            </ScrollView>
        )
    }
}

export default DetailScreen;