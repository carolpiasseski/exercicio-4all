import { StyleSheet } from 'react-native';
import colors from '../../constants/colors';

const styles = StyleSheet.create({
    scrollView: {
        flex: 1,
        backgroundColor: colors.gallery
    },
    photoImageBackground: {
        flex: 1,
        height: 210
    },
    logoContainer: {
        position: 'absolute', 
        backgroundColor: colors.white, 
        right: 20, 
        top: 160, 
        borderRadius: 40, 
        alignItems: 'center', 
        justifyContent: 'center'
    },
    logoImage: {
        flex: 1, 
        height: 80, 
        width: 80
    },
    titleText: {
        flex: 1,
        fontSize: 24,
        color: colors.geebung,
        padding: 10,
        fontWeight: '400'
    },
    buttonBarContainer: {
        flex: 1,
        flexDirection: 'row',
        height: 55,
        backgroundColor: colors.white,
    },
    descriptionContainer: {
        flex: 1,
        minHeight: 100,
        marginTop: 2,
        backgroundColor: colors.white
    },
    descriptionText: {
        fontSize: 14,
        color: colors.geebung,
        padding: 10
    },
    mapContainer: {
        flex: 1
    },
    mapStyle: {
        flex: 1,
        height: 150,
        marginTop: 2
    },
    addressContainer: {
        flex: 1, 
        height: 24, 
        backgroundColor: colors.geebung, 
        alignItems: 'center', 
        flexDirection: 'row', 
        marginBottom: 2
    },
    addressText: {
        flex: 1, 
        color: colors.white, 
        marginRight: 60, 
        textAlign: 'right'
    },
    adressContainerLogo: {
        position: 'absolute', 
        backgroundColor: colors.white, 
        right: 10, 
        bottom: 2, 
        borderRadius: 20, 
        alignItems: 'center', 
        justifyContent: 'center'
    },
    addressImage: {
        flex: 1, 
        height: 40, 
        width: 40
    },
    emptyContainer: {
        flex: 1, 
        height: 200, 
        marginTop: 2
    }
});

export default styles;