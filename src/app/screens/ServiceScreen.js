import React, { Component } from 'react';
import { View } from 'react-native';
import colors from '../constants/colors';

class ServiceScreen extends Component {
    static navigationOptions = {
        title: 'Serviços',
        headerStyle: { backgroundColor: colors.geebung, height: 40 },
        headerTintColor: colors.white,
        headerTitleStyle: {  flex: 1, fontSize: 13, fontWeight: 'bold', textAlign: 'center' },
        headerRight: <View style={{ flex: 1 }}/>
    };
    
    render() {
        return(
            <View styles={{ flex: 1 }}/>
        )
    }
}

export default ServiceScreen;