import React, { Component } from 'react';
import { FlatList, TouchableOpacity, Text, StyleSheet } from 'react-native';
import LoadScreen from '../components/LoadScreen';
import ErrorScreen from '../components/ErrorScreen';
import colors from '../constants/colors';
import axios from 'axios';

class HomeScreen extends Component {
    state = {
        data: [],
        error: false,
        loading: true 
    }
    
    loadData = async () => {
        this.setState({
            loading: true
        })

        try { 
            const response = await axios.get('http://dev.4all.com:3003/tarefa')
            this.setState({
                loading: false,
                error: false,
                data: response.data.lista
            })
        } catch (err) {
            this.setState({
                loading: false,
                error: true
            })
        }

    }

    componentWillMount() {
        this.loadData()
    }
    
    static navigationOptions = {
        title: 'Lista',
        headerStyle: { backgroundColor: colors.geebung, height: 40 },
        headerTintColor: colors.white,
        headerTitleStyle: { flex: 1, fontSize: 13, fontWeight: 'bold', textAlign: 'center' },
    };

    render() {
        if(this.state.loading){
            return <LoadScreen />
        }
        if(this.state.error){
            return <ErrorScreen isRefreshing={this.state.loading} onRefresh={this.loadData} />    
        }
        
        return (
            <FlatList
                style={styles.list}
                data={ this.state.data }
                keyExtractor={(item, index) => String(index)} // chave unica conforme docum.
                renderItem={({item}) => 
                    <TouchableOpacity
                        style={styles.touchable} 
                        onPress={() => this.props.navigation.navigate('Detail', {id: item})}>
                        <Text style={styles.idText}>{item}</Text>
                    </TouchableOpacity>
                }
            />
        ) 
    }
}

const styles = StyleSheet.create({
    list: {
        marginTop: 15
    },
    touchable: {
        backgroundColor: colors.geebung,
        height: 45,
        borderWidth: 1,
        margin: 5,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 4 
    },
    idText: {
        fontSize: 24,
        fontWeight: '900',
        color: colors.white,
        textAlign: 'center'
    },
});

export default HomeScreen;